from typing import Any
from requests import get as get_request
from config import spacex_api_url

def get_all_capsules() -> Any :
    """Fonction pour recuperer toutes les capsules sur SpaceX API"""
    try:
        response = get_request('{}capsules/'.format(spacex_api_url)) # Requete permettant de recuperer la liste des capsules sur le Serveur SpaceX
        return response.json()
    except Exception as exc:
        return [] # Retourner une liste vide si une erreur survient

def get_one_capsules_by_serial(capsule_serial: str) -> Any :
    """Fonction pour recuperer une capsule par sa série sur SpaceX API"""
    try:
        response = get_request('{}capsules/{}/'.format(spacex_api_url, capsule_serial)) # Requete permettant de recuperer une capsule sur le Serveur SpaceX par sa serie

        return response.json()
    except Exception as exc:
        return {}  # Retourner un dictionnaire vide si une erreur survienne

