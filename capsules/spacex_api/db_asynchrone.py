from typing import List
import datetime
from capsules.models import Capsule, Mission

def save_liste_capsules(liste: List):
    """Fonction permettant d'enregistrer les capsules avec leurs missions dans la base de données"""
    for elt in liste: # Parcours de la liste des capsules
        try:
            if not Capsule.objects.filter(capsule_serial=elt.get("capsule_serial")): # Verifier si une capsule n'a pas encore cette serie
                """Si aucune capsule n'a cette serie"""
                capsule = Capsule()
                capsule.capsule_serial = elt.get("capsule_serial")
                capsule.capsule_id = elt.get("capsule_id")
                capsule.status = elt.get("status")
                capsule.original_launch = elt.get("original_launch")
                if elt.get("original_launch_unix"): # Verifier si original_launch_unix n'est pas null
                    """Si original_launch_unix n'est pas null"""
                    capsule.original_launch_unix = datetime.datetime.fromtimestamp(elt["original_launch_unix"]) # Convertir le timestamp (Integer) en datetime
                capsule.landings = elt.get("landings") if elt.get("landings") else 0 # Si landings existe et n'est pas nulle, retourner 'sa valeur', sinon, retourner 0
                capsule.type = elt.get("type")
                capsule.reuse_count = elt.get("reuse_count") if elt.get("reuse_count") else 0 # Si reuse_count existe et n'est pas nulle, retourner 'sa valeur', sinon, retourner 0
                capsule.details = elt.get("details")

                capsule.save() # enregistrer la capsule

            capsule = Capsule.objects.get(capsule_serial=elt.get("capsule_serial")) # recuperer une capsule par sa serie
            if elt.get("missions"): # Verifier si la capsule a au moins une mission
                for eltMission in elt.get("missions"):  # Parcours de la liste des missions d'une capsule
                    try:
                        if not Mission.objects.filter(name=eltMission.get("name")): # Verifier si une mission n'a pas encore ce nom
                            mission = Mission()
                            mission.capsule = capsule
                            mission.name = eltMission.get("name")
                            mission.flight = eltMission.get("flight") if eltMission.get("flight") else 0
                            mission.save() # enregistrer la mission

                    except Exception as exc:
                        print(exc, eltMission.get("name"))

        except Exception as exc:
            print(exc, elt.get("capsule_serial"))


